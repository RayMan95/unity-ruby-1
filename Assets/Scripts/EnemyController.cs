﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float speed;

    public bool vertical;
    public bool positiveDirection;
    int direction;

    public int maxHealth = 3;
    int hits = 0;

    Rigidbody2D rigidbody2d;
    Animator animator;

    bool broken;
    public ParticleSystem smokeEffect;
    public GameObject hitEffectParticlePrefab;

    AudioSource[] audioSources; // [walking, one-off]
    public AudioClip[] oneOffClips; // [hit1, hit2, fixed]

    // Start is called before the first frame update
    void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        audioSources = GetComponents<AudioSource>();

        broken = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!broken)
        {
            return;
        }

        Vector2 position = rigidbody2d.position;

        if (positiveDirection)
        {
            direction = 1;
        }
        else
        {
            direction = -1;
        }

        if (vertical)
        {
            position.y = position.y + speed * direction * Time.deltaTime;
            
            animator.SetFloat("Move X", 0);
            animator.SetFloat("Move Y", direction);
        }
        else 
        {
            position.x = position.x + speed * direction * Time.deltaTime;

            animator.SetFloat("Move X", direction);
            animator.SetFloat("Move Y", 0);
        }

        rigidbody2d.MovePosition(position);
    }

    public void Hit(int amount, Vector2 hitLocation)
    {
        GameObject hitEffect = Instantiate(hitEffectParticlePrefab, hitLocation, Quaternion.identity);

        if (hits+1 == maxHealth)
        {
            this.Fix();
        }
        else
        {
            hits++;
            if (hits % 2 == 0)
            {
                this.PlaySound("Hit 1");
            }
            else
            {
                this.PlaySound("Hit 2");
            }
        }
        
    }

    void Fix()
    {
        broken = false;
        rigidbody2d.simulated = false;

        this.PlaySound("Fixed");
        audioSources[0].Stop();
        animator.SetTrigger("Fixed");
        smokeEffect.Stop();
    }

    void PlaySound(string eventName)
    {
        switch(eventName)
        {
            case "Hit 1":
                audioSources[1].PlayOneShot(oneOffClips[0]);
                break;
            case "Hit 2":
                audioSources[1].PlayOneShot(oneOffClips[1]);
                break;
            case "Fixed":
                audioSources[1].PlayOneShot(oneOffClips[2]);
                break;
        }
    }
}
